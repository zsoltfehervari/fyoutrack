
FUT = {

    oldHREF: null,
    issues: ['issues?preview=','/issue/'],


    init: () => {

        setTimeout(() => {
 
            const observer = new MutationObserver(mutations => {
                if (FUT.oldHREF !== document.location.href) {
                  FUT.oldHREF = document.location.href;
                  
                  if(FUT.issues.some(i => window.location.href.includes(i))){
      
                      setTimeout(FUT.insertCreateSubtaskButton, 500);
        
                  }
      
                }
              });
      
            observer.observe(document.querySelector("body"), { childList: true, subtree: true });


        },500);

    },

    triggerCreateSubtaskKeyboardShortcut: () => {
        
        console.log('click');
        
        window.dispatchEvent(new KeyboardEvent('keydown', {
            key: 'Insert',
            code: 'Insert',
            ctrlKey: true,
            altKey: true,
        }));

    },

    insertCreateSubtaskButton: () => {

        const toolBar = document.querySelector('[class^=summaryToolbar_]');

        if(!toolBar){
            return;
        }

        const span = document.createElement('span');

        span.setAttribute('data-test', 'ring-select tag-button');
        span.setAttribute('title', 'Create Subtask');
        
        span.insertAdjacentHTML('beforeend',`
            <span aria-label="Create Subtask" role="tooltip" data-test="ring-tooltip" data-test-title="Create Subtask">
                    <button type="button" tabindex="0" data-test="addTag" class="ring-ui-button_aba4 ring-ui-heightS_b28d ring-ui-withIcon_ef77 ring-ui-withNormalIcon_aaca">
                        <span class="ring-ui-content_b2b8"><span class="ring-ui-icon_e878"><span class="ring-ui-icon_aaa7">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" class="glyph_ffd8"><g><polygon points="14 7.11 8.9 7.11 8.9 2.01 7.1 2.01 7.1 7.11 2 7.11 2 8.91 7.1 8.91 7.1 13.99 8.9 13.99 8.9 8.91 14 8.91 14 7.11"></polygon></g></svg>
                        </span>
                    </span>
                </span>
                    </button>
                <span role="presentation"></span>
                </span>
            <span role="presentation"></span>
        `);

        span.addEventListener('click', FUT.triggerCreateSubtaskKeyboardShortcut, false);
        toolBar.appendChild(span);

    },
}

window.addEventListener("load",FUT.init,false);